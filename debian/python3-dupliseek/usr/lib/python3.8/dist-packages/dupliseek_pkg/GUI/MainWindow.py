import hashlib
import os

import numpy as np
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QMainWindow, QToolBar, QAction, QFileDialog, QProgressBar, QMessageBox, QStyle, \
	QLabel

from dupliseek_pkg.GUI import is_dark_theme, get_stylesheet, get_icon, tr, gui_scale
from dupliseek_pkg.GUI.Ribbon.RibbonButton import RibbonButton
from dupliseek_pkg.GUI.Ribbon.RibbonTextbox import RibbonTextbox
from dupliseek_pkg.GUI.Ribbon.RibbonWidget import RibbonWidget
from dupliseek_pkg.GUI.Widgets.DupliCompareView import DupliCompareDock
from dupliseek_pkg.GUI.Widgets.DuplicateTable import DupliTableDock
from dupliseek_pkg.GUI.Widgets.MainWidget import MainWidget
from dupliseek_pkg.GUI.Widgets.TreeView import TreeViewDock

import cv2


class MainWindow(QMainWindow):
	def __init__(self, app):
		QMainWindow.__init__(self, None)
		self.setMinimumWidth(1024*gui_scale())
		self.setMinimumHeight(600*gui_scale())
		self.setWindowIcon(get_icon("compare"));

		self.setWindowTitle("DupliSeek")
		self._root_folder = ""
		self._images = []
		self._image_endings = [".jpg", ".png", ".webp", ".tiff"]
		self._hash_matches = {}
		self._img_matches = {}
		self._thumbs = {}
		self._matches = []

		self._image_compare_thresshold = 160000

		self.open_action = self.add_action("Open\nFolder", QStyle.SP_DirOpenIcon, "Open Folder", True, self.on_open_folder, QKeySequence.Open)
		self.find_action = self.add_action("Find\nDuplicates", "zoomfit", "Find Duplicate images", True, self.on_find, QKeySequence.Find)
		self.trash_action = self.add_action("Put Image\nin Trash", QStyle.SP_TrashIcon, "Put selected image in trash", True, self.on_trash,QKeySequence.Delete)
		self.hash_compare_action = self.add_action("Hash\nCompare", "compare", "compare images by hash", True, self.on_hash_compare, checkable=True)
		self.image_compare_action = self.add_action("Image\nCompare", "compare", "Compare images by content", True, self.on_image_compare, checkable=True)
		self.about_action = self.add_action("About\nApp", QStyle.SP_MessageBoxInformation, "About this application", True, self.on_about)

		self._img_diff_percentage = RibbonTextbox("2.0", self.on_img_diff_percentage_changed, 60)
		self._img_diff_percentage_label = QLabel("Threshold %")
		self._img_diff_percentage.setEnabled(False)
		self._img_diff_percentage_label.setEnabled(False)

		self.hash_compare_action.setChecked(True)
		self.trash_action.setEnabled(False)
		self.find_action.setEnabled(False)

		self._ribbon = QToolBar(self)
		if is_dark_theme():
			self._ribbon.setStyleSheet(get_stylesheet("ribbon_dark"))
		else:
			self._ribbon.setStyleSheet(get_stylesheet("ribbon"))
		self._ribbon.setObjectName("ribbonWidget")
		self._ribbon.setWindowTitle("Ribbon")
		self.addToolBar(self._ribbon)
		self._ribbon.setMovable(False)
		self._ribbon_widget = RibbonWidget(self)
		self._ribbon_widget.currentChanged.connect(self.on_ribbon_changed)
		self._ribbon.addWidget(self._ribbon_widget)
		self.init_ribbon()

		self._tree_view_dock = TreeViewDock(self)
		self.addDockWidget(Qt.LeftDockWidgetArea, self._tree_view_dock)

		self._dupli_table_dock = DupliTableDock(self)
		self.addDockWidget(Qt.RightDockWidgetArea, self._dupli_table_dock)

		self._dupli_compare_dock = DupliCompareDock(self)
		self.addDockWidget(Qt.BottomDockWidgetArea, self._dupli_compare_dock)

		self.setCentralWidget(MainWidget(self))

		self.statusBar().showMessage("Ready")
		self._progress_bar = QProgressBar()
		#self._progress_bar.setMaximumHeight(20)
		#self._progress_bar.setMaximumWidth(300)
		self._progress_bar.setTextVisible(False)
		self.statusBar().addPermanentWidget(self._progress_bar, 0)


	def on_ribbon_changed(self):
		pass

	def init_ribbon(self):
		self.init_home_tab()

	def init_home_tab(self):
		home_tab = self._ribbon_widget.add_ribbon_tab("Home")
		file_pane = home_tab.add_ribbon_pane("File")
		file_pane.add_ribbon_widget(RibbonButton(self, self.open_action, True))
		file_pane.add_ribbon_widget(RibbonButton(self, self.find_action, True))
		file_pane.add_ribbon_widget(RibbonButton(self, self.trash_action, True))
		compare_pane = home_tab.add_ribbon_pane("Compare")
		compare_pane.add_ribbon_widget(RibbonButton(self, self.hash_compare_action, True))
		compare_pane.add_ribbon_widget(RibbonButton(self, self.image_compare_action, True))

		grid = compare_pane.add_grid_widget(200)
		grid.addWidget(self._img_diff_percentage_label, 1, 1)
		#grid.addWidget(QLabel("Text box 2"), 2, 1)
		#grid.addWidget(QLabel("Text box 3"), 3, 1)
		grid.addWidget(self._img_diff_percentage, 1, 2)
		#grid.addWidget(self._text_box2, 2, 2)
		#grid.addWidget(self._text_box3, 3, 2)

		info_pane = home_tab.add_ribbon_pane("Info")
		info_pane.add_ribbon_widget(RibbonButton(self, self.about_action, True))
		home_tab.add_spacer()

	def add_action(self, caption, icon_name, status_tip, icon_visible, connection, shortcut=None, checkable=False):
		action = QAction(get_icon(icon_name), tr(caption, "ribbon"), self)
		action.setStatusTip(tr(status_tip, "ribbon"))
		action.triggered.connect(connection)
		action.setIconVisibleInMenu(icon_visible)
		if shortcut is not None:
			action.setShortcuts(shortcut)
		action.setCheckable(checkable)
		self.addAction(action)
		return action

	def on_open_folder(self):
		path = QFileDialog.getExistingDirectory(self, tr("Open Directory"), os.path.expanduser("~"), QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks);
		self._tree_view_dock.set_folder(path)
		self._root_folder = path
		self.find_action.setEnabled(True)

	def on_find(self):
		files = []

		path = self._root_folder
		for r, d, f in os.walk(path):
			for file in f:
				if self.is_picture(file):
					files.append(os.path.join(r, file))

		self._images = files
		self._hash_matches = {}
		self._matches = []
		self._img_matches = {}

		print ("hashing")
		counter = 0
		self._progress_bar.setValue(counter/max(len(files), 1))
		for file in self._images:
			md5sum = hashlib.md5(open(file,'rb').read()).hexdigest()
			self.store_hash_match(md5sum, file)
			#print(md5sum)
			counter += 1
			self._progress_bar.setValue(100*counter / len(files))
			self.statusBar().showMessage("Hasing: " + file)
		matching_keys = []
		self._thumbs = {}
		if self.image_compare_action.isChecked():
			print("image comparing")
			print("loading")
			counter = 0
			for hash_match in self._hash_matches:
				# images are loaded and scaled to thumbnail in grayscale to conserve memory and processing time.
				file = self._hash_matches[hash_match][0]
				img = cv2.imread(file)
				if not type(img) is None:
					#print(file)
					thumb = cv2.resize(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),(200, 150))
					self._thumbs[hash_match] = np.int32(thumb)
				counter += 1
				self._progress_bar.setValue(100 * counter / len(self._hash_matches))
				self.statusBar().showMessage("Loading: " + file)
			counter = 0
			print("subtract")
			keys = list(self._thumbs.keys())
			for i in range(len(keys)):
				counter += 1
				if keys[i] in matching_keys:
					continue
				thumbi = self._thumbs[keys[i]]
				# Images are subtracted from each other and the resulting sum is compared.
				for j in range(i+1, len(keys)):
					thumbj = self._thumbs[keys[j]]
					result = np.sum(np.absolute(thumbi-thumbj))
					#print(str(result))
					if result < self._image_compare_thresshold: # if difference is smaller than thresshold
						if not keys[i] in self._img_matches:
							self._img_matches[keys[i]] = []
						self._img_matches[keys[i]].append(keys[j])
						matching_keys.append(keys[j]) # adding key j to matching_keys to filter it from the result
																					# . since its merged with key i

				self._progress_bar.setValue(100 * counter / len(keys))
				self.statusBar().showMessage("comparing: " + self._hash_matches[keys[i]][0])

		for hash_match in self._hash_matches:
			if hash_match in matching_keys:
				#print("matched: " + str(self._hash_matches[hash_match]))
				continue
			imgarray = []
			if len(self._hash_matches[hash_match]) > 1:
				imgarray = self._hash_matches[hash_match]
			if hash_match in self._img_matches:
				otherhashes = self._img_matches[hash_match]
				for otherhash in otherhashes:
					for img in self._hash_matches[otherhash]:
						imgarray.append(img)
				if len(self._hash_matches[hash_match]) == 1:
					for img in self._hash_matches[hash_match]:
						imgarray.append(img)
			if len(imgarray) > 0:
				self._matches.append(imgarray)

		message = str(len(self._matches)) + " matches found"
		print(message)
		self.statusBar().showMessage(message, 20000)

		self._dupli_table_dock.set_duplicates(self._matches)

	def store_hash_match(self, md5sum, path):
		if md5sum in self._hash_matches:
			self._hash_matches[md5sum].append(path)
		else:
			self._hash_matches[md5sum] = [path]

	def is_picture(self, file):
		for image_ending in self._image_endings:
			if file.endswith(image_ending):
				return True

	def on_tree_selection_changed(self, selected_item):
		self.centralWidget().show_picture(selected_item)
		#self._dupli_compare_dock.set_icons([selected_item, selected_item, selected_item])

	def on_dupli_table_selection_changed(self, duplicates):
		self.centralWidget().show_picture(duplicates[0])
		self._dupli_compare_dock.set_icons(duplicates)

	def on_dupli_compare_selection_changed(self, image):
		self.centralWidget().show_picture(image)

	def on_image_compare(self):
		enabled = self.image_compare_action.isChecked()
		self._img_diff_percentage.setEnabled(enabled)
		self._img_diff_percentage_label.setEnabled(enabled)

	def on_hash_compare(self):
		self.hash_compare_action.setChecked(True)

	def on_trash(self):
		pass

	def on_img_diff_percentage_changed(self):
		try:
			value = float(self._img_diff_percentage.text())
			self._image_compare_thresshold = 255*150*200*value/100
			#self._image_compare_thresshold = min(100, self._image_compare_thresshold)
			self._image_compare_thresshold = max(0, self._image_compare_thresshold)
		except ValueError:
			self._img_diff_percentage.setText(str(100*self._image_compare_thresshold/(255*150*200)))
		print("thresshold = " +  str(self._image_compare_thresshold))

	def on_about(self):
		title = self.windowTitle()
		message = self.windowTitle() + "" \
				" is an application made by:" \
				"\nMagnus Jørgensen" \
				"\nPrimož Ajdišek" \
				"\nIt is licensed under MIT license." \
				"\nSource code can be found at: \nhttps://gitlab.com/dupliseek-group/dupliseek"
		QMessageBox.about(self, title, message)

