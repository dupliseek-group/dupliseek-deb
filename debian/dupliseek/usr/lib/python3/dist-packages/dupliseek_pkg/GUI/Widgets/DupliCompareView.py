from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDockWidget, QListWidget, QListWidgetItem

from dupliseek_pkg.GUI import tr, gui_scale


class DupliCompareDock(QDockWidget):
    def __init__(self, main_window):
        QDockWidget.__init__(self, main_window)
        self._main_window = main_window
        self.setWindowTitle(tr("Compare"))
        self.setObjectName("DupliTableDock")
        self.setMinimumHeight(220*gui_scale())
        self._icon_view = QListWidget(self)
        self._icon_view.setViewMode(QListWidget.IconMode)
        self._icon_view.setIconSize(QSize(200*gui_scale(),200*gui_scale()))
        self.setWidget(self._icon_view)
        self._icon_view.selectionModel().selectionChanged.connect(self.on_selection_changed)
        self._images = []

    def set_icons(self, images):
        self._images = images
        self._icon_view.clear()
        for image in images:
            path = image.replace(self._main_window._root_folder, '', 1)
            self._icon_view.addItem(QListWidgetItem(QIcon(image), path))


    def on_selection_changed(self, selection):
        for selrange in selection:
            for sel in selrange.indexes():
                image = self._images[sel.row()]
                self._main_window.on_dupli_compare_selection_changed(image)
                return
